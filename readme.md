Make sure:
  Metamask installed
  Rinkeby network is selected in metamask
  Account has enough ether


From `deploy and run tracsaction tab`,
    select environment as `injected web3`
    add your rinky by account

TxAddress: https://rinkeby.etherscan.io/tx/0x3349da25727612e32d292b2fd1f1d2ac5d07b35b9b0f62f356f60d3aa65c6241

TxHash: 0x3349da25727612e32d292b2fd1f1d2ac5d07b35b9b0f62f356f60d3aa65c6241

From account: 0x612a306d2707cf0B49A5d76594482f7AfCda506c

Contract address: 0x8C9815E2372bC6F9Dec915751B31666aB08b2edb

Contract link: https://rinkeby.etherscan.io/address/0x8c9815e2372bc6f9dec915751b31666ab08b2edb